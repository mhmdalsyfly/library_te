package com.mhammed_khaled_alseqaly.library_temohammed.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;

import com.mhammed_khaled_alseqaly.library_temohammed.R;

public class UpdateDialogClass extends Dialog implements android.view.View.OnClickListener {

    Context context;


    public UpdateDialogClass(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_update);



    }

    @Override
    public void onClick(View view) {

    }
}
