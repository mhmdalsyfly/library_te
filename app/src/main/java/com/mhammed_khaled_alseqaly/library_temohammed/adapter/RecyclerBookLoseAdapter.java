package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Metaphor;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class RecyclerBookLoseAdapter extends RecyclerView.Adapter<RecyclerBookLoseAdapter.Reference> {

    ArrayList<Book> listBook = new ArrayList<Book>();
    Context mContext;
    Activity activity;

    public RecyclerBookLoseAdapter(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
    }

    public void addAll(ArrayList<Book> newBook) {
        int initialSize = listBook.size();
        this.listBook.addAll(newBook);
        notifyItemRangeInserted(initialSize, newBook.size());
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_lose_book, parent, false);
        return new Reference(v);
    }

    public ArrayList<Book> getAllElements(){
        return listBook;
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final Book book = listBook.get(position);
        switch (book.getStatus()) {
            case "متاح":
                holder.view.setBackgroundResource(R.color.green);
                break;
            case "غير مرجع":
                holder.view.setBackgroundResource(R.color.yellow);
                break;
            case "مفقود":
                holder.view.setBackgroundResource(R.color.red);
                break;
            case "تالف":
                holder.view.setBackgroundResource(R.color.blue);
                break;
            case "مسحوب":
                holder.view.setBackgroundResource(R.color.zahry);
                break;
        }

        holder.genNum.setText(book.getGen_number()+"");
        holder.name_book.setText(book.getName_book());

        if (book.getAuthor_book() == null || book.getAuthor_book().equals("")) {
            holder.author_book.setText("اسم المؤلف غير موجود");
        } else {
            holder.author_book.setText(book.getAuthor_book());
        }

    }

    @Override
    public int getItemCount() {
        return listBook.size();
    }

    public String getSizeArray() {
        return listBook.size() + 1 + "";
    }

    public void reListData() {
        listBook.clear();
        notifyDataSetChanged();
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView name_book, author_book,genNum;
        ImageView ic_delete, ic_edit;
        View view;

        public Reference(@NonNull View itemView) {
            super(itemView);
            name_book = itemView.findViewById(R.id.name_book);
            genNum = itemView.findViewById(R.id.genNum);
            author_book = itemView.findViewById(R.id.name_author);
            view = itemView.findViewById(R.id.view);
        }
    }
}
