package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;

import java.util.ArrayList;

public class RecyclerTeacherMetaphorAdapter extends RecyclerView.Adapter<RecyclerTeacherMetaphorAdapter.Reference> {

    ArrayList<Teacher> listTeacher = new ArrayList<Teacher>();
    Context mContext;
    Activity activity;

    public RecyclerTeacherMetaphorAdapter(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
    }

    public void addAll(ArrayList<Teacher> newTeacher) {
        int initialSize = listTeacher.size();
        this.listTeacher.addAll(newTeacher);
        notifyItemRangeInserted(initialSize, newTeacher.size());
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_more_pre, parent, false);
        return new Reference(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final Teacher teacher = listTeacher.get(position);

        holder.id.setText(teacher.getId() + "");
        holder.name.setText(teacher.getName_teacher() + "");
        holder.count_book.setText("عدد الكتب : " + teacher.getNumber_books());

    }

    @Override
    public int getItemCount() {
        return listTeacher.size();
    }

    public String getSizeArray() {
        return listTeacher.size() + 1 + "";
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView id, name, count_book;

        public Reference(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            name = itemView.findViewById(R.id.name);
            count_book = itemView.findViewById(R.id.count_book);
        }
    }
}
