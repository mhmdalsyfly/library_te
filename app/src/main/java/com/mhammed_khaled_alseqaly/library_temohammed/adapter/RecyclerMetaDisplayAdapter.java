package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Metaphor;

import java.util.ArrayList;

public class RecyclerMetaDisplayAdapter extends RecyclerView.Adapter<RecyclerMetaDisplayAdapter.Reference> {

    ArrayList<Metaphor> listMetaphor = new ArrayList<Metaphor>();
    Context mContext;
    Activity activity;

    public RecyclerMetaDisplayAdapter(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
    }

    public void addAll(ArrayList<Metaphor> newMetaphor) {
        int initialSize = listMetaphor.size();
        this.listMetaphor.addAll(newMetaphor);
        notifyItemRangeInserted(initialSize, newMetaphor.size());
    }
    public void reListData() {
        listMetaphor.clear();
        notifyDataSetChanged();
    }

    public ArrayList<Metaphor> getAllElements() {
        return listMetaphor;
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_metaphors_display, parent, false);
        return new Reference(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final Metaphor metaphor = listMetaphor.get(position);

        holder.genNum.setText(metaphor.getGen_number()+"");
        holder.name_bk.setText(metaphor.getName_book() + "");
        holder.name_per.setText(metaphor.getName_person() + "");
        holder.date.setText(metaphor.getDate_metaphor() + "");

    }

    @Override
    public int getItemCount() {
        return listMetaphor.size();
    }

    public String getSizeArray() {
        return listMetaphor.size() + 1 + "";
    }

    public void addMetaphor(Metaphor metaphor) {
        listMetaphor.add(metaphor);
        notifyDataSetChanged();
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView name_per, name_bk, date,genNum;

        public Reference(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            genNum = itemView.findViewById(R.id.genNum);
            name_per = itemView.findViewById(R.id.name_per);
            name_bk = itemView.findViewById(R.id.name_bk);
        }
    }
}
