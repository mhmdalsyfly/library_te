package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Return;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Return;

import java.util.ArrayList;

public class RecyclerBookReturnAdapter extends RecyclerView.Adapter<RecyclerBookReturnAdapter.Reference> {

    ArrayList<Return> listReturn = new ArrayList<Return>();
    Context mContext;
    Activity activity;

    public RecyclerBookReturnAdapter(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
    }

    public void addAll(ArrayList<Return> newReturn) {
        int initialSize = listReturn.size();
        this.listReturn.addAll(newReturn);
        notifyItemRangeInserted(initialSize, newReturn.size());
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_return_book, parent, false);
        return new Reference(v);
    }

    public ArrayList<Return> getAllElements() {
        return listReturn;
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final Return aReturn = listReturn.get(position);

        holder.view.setBackgroundResource(R.color.green);
        holder.id_person.setText(aReturn.getId_person()+"");
        holder.genNum.setText(aReturn.getGen_number()+"");
        holder.name_book.setText(aReturn.getName_book());
        holder.name_pre.setText(aReturn.getName_per());
        holder.date_return.setText(aReturn.getReturn_date());

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                deleteBookInDatabase(aReturn,position);
                return true;
            }
        });

    }

    private void deleteBookInDatabase(final Return data, final int i) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child("return_books").orderByChild("id_row").equalTo(data.getId_row());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    new AlertDialog.Builder(activity)
                            .setTitle("حذف المدخلات")
                            .setMessage("هل أنت متأكد أنك تريد حذف الكتاب؟?!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (dataSnapshot != null) {
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            appleSnapshot.getRef().removeValue();
                                        }
                                        Toast.makeText(mContext, "تمت عملية الحذف بنجاح ", Toast.LENGTH_SHORT).show();
                                        listReturn.remove(i);
                                        notifyItemRemoved(i);
                                        notifyItemRangeChanged(i, listReturn.size());
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(mContext, "لم تتم عملية الحذف بنجاح ؟!..", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return listReturn.size();
    }

    public String getSizeArray() {
        return listReturn.size() + 1 + "";
    }

    public void reListData() {
        listReturn.clear();
        notifyDataSetChanged();
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView name_book, name_pre, date_return,genNum,id_person;
        View view;

        public Reference(@NonNull View itemView) {
            super(itemView);
            id_person = itemView.findViewById(R.id.id_person);
            genNum = itemView.findViewById(R.id.genNum);
            name_book = itemView.findViewById(R.id.name_bok);
            name_pre = itemView.findViewById(R.id.name_per);
            date_return = itemView.findViewById(R.id.date_return);
            view = itemView.findViewById(R.id.view);
        }
    }
}
