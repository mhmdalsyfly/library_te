package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.MetaphorDisplay;
import com.mhammed_khaled_alseqaly.library_temohammed.model.MetaphorDisplay;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Return;

import java.util.ArrayList;

public class RecyclerMetaDisplayAdapter2 extends RecyclerView.Adapter<RecyclerMetaDisplayAdapter2.Reference> {

    ArrayList<MetaphorDisplay> listMetaphorD = new ArrayList<MetaphorDisplay>();
    Context mContext;
    Activity activity;

    public RecyclerMetaDisplayAdapter2(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
    }

    public void addAll(ArrayList<MetaphorDisplay> newMetaphorD) {
        int initialSize = listMetaphorD.size();
        this.listMetaphorD.addAll(newMetaphorD);
        notifyItemRangeInserted(initialSize, newMetaphorD.size());
    }

    public void reListData() {
        listMetaphorD.clear();
        notifyDataSetChanged();
    }

    public ArrayList<MetaphorDisplay> getAllElements() {
        return listMetaphorD;
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_metaphors_display2, parent, false);
        return new Reference(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final MetaphorDisplay metaphorDisplay = listMetaphorD.get(position);

        holder.id_person.setText(metaphorDisplay.getId_person()+"");
        holder.name_bk.setText(metaphorDisplay.getName_book() + "");
        holder.name_per.setText(metaphorDisplay.getName_person() + "");
        holder.date.setText(metaphorDisplay.getDate_metaphor() + "");
        holder.genNum.setText(metaphorDisplay.getGen_number() + "");

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                deleteBookInDatabase(metaphorDisplay, position);
                return true;
            }
        });

    }

    private void deleteBookInDatabase(final MetaphorDisplay data, final int i) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child("metaphors_display").orderByChild("id_row").equalTo(data.getId_row());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    new AlertDialog.Builder(activity)
                            .setTitle("حذف المدخلات")
                            .setMessage("هل أنت متأكد أنك تريد حذف الكتاب؟?!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (dataSnapshot != null) {
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            appleSnapshot.getRef().removeValue();
                                        }
                                        Toast.makeText(mContext, "تمت عملية الحذف بنجاح ", Toast.LENGTH_SHORT).show();
                                        listMetaphorD.remove(i);
                                        notifyItemRemoved(i);
                                        notifyItemRangeChanged(i, listMetaphorD.size());
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(mContext, "لم تتم عملية الحذف بنجاح ؟!..", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMetaphorD.size();
    }

    public String getSizeArray() {
        return listMetaphorD.size() + 1 + "";
    }

    public void addMetaphor(MetaphorDisplay MetaphorDisplay) {
        listMetaphorD.add(MetaphorDisplay);
        notifyDataSetChanged();
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView name_per, name_bk, date,genNum,id_person;

        public Reference(@NonNull View itemView) {
            super(itemView);
            id_person = itemView.findViewById(R.id.id_person);
            date = itemView.findViewById(R.id.date);
            genNum = itemView.findViewById(R.id.genNum);
            name_per = itemView.findViewById(R.id.name_per);
            name_bk = itemView.findViewById(R.id.name_bk);
        }
    }
}
