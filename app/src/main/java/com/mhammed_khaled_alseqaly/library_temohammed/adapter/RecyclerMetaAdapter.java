package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Metaphor;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Return;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class RecyclerMetaAdapter extends RecyclerView.Adapter<RecyclerMetaAdapter.Reference> {

    DatabaseReference root = FirebaseDatabase.getInstance()
            .getReference("database").child("metaphors");
    DatabaseReference root_teacher = FirebaseDatabase.getInstance()
            .getReference("database").child("teachers");
    DatabaseReference root_books = FirebaseDatabase.getInstance()
            .getReference("database").child("books");
    DatabaseReference root_students = FirebaseDatabase.getInstance()
            .getReference("database").child("students");
    DatabaseReference root_return_book = FirebaseDatabase.getInstance()
            .getReference("database").child("return_books");
    DatabaseReference root_no_return_book = FirebaseDatabase.getInstance()
            .getReference("database").child("no_return_books");

    ArrayList<Metaphor> listMetaphor = new ArrayList<Metaphor>();
    Context mContext;
    Activity activity;

    public RecyclerMetaAdapter(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
    }

    public void addAll(ArrayList<Metaphor> newMetaphor) {
        int initialSize = listMetaphor.size();
        this.listMetaphor.addAll(newMetaphor);
        notifyItemRangeInserted(initialSize, newMetaphor.size());
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_metaphors, parent, false);
        return new Reference(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final Metaphor metaphor = listMetaphor.get(position);

        holder.name_bk.setText(metaphor.getName_book() + "");
        holder.name_per.setText(metaphor.getName_person() + "");
        holder.date.setText(metaphor.getDate_metaphor() + "");

        String getDateOld = metaphor.getDate_metaphor().replace("٠", "0")
                .replace("١", "1").replace("٢", "2").replace("٣", "3")
                .replace("٤", "4").replace("٥", "5").replace("٦", "6")
                .replace("٧", "7").replace("٨", "8").replace("٩", "9")
                .replace("٫", ".").replace("-", "/");

        String dateNow = new SimpleDateFormat("yyyy/M/dd", Locale.getDefault()).format(new Date())
                .replace("٠", "0")
                .replace("١", "1").replace("٢", "2").replace("٣", "3")
                .replace("٤", "4").replace("٥", "5").replace("٦", "6")
                .replace("٧", "7").replace("٨", "8").replace("٩", "9")
                .replace("٫", ".").replace("-", "/");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/M/dd");
        try {
            Date date1 = simpleDateFormat.parse(getDateOld);
            Date date2 = simpleDateFormat.parse(dateNow);
            long duration = printDifference(date1, date2);
            holder.duration.setText(duration + " يوم");
            switch ((int) duration) {
                case 0:
                    holder.duration.setBackgroundResource(R.color.green);
                    break;
                case 1:
                    holder.duration.setBackgroundResource(R.color.green);
                    break;
                case 2:
                    holder.duration.setBackgroundResource(R.color.yellow);
                    break;
                case 3:
                    holder.duration.setBackgroundResource(R.color.red);
                    break;
                default:
                    holder.duration.setBackgroundResource(R.color.red);
                    break;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
        return elapsedDays;
    }

    @Override
    public int getItemCount() {
        return listMetaphor.size();
    }

    public String getSizeArray() {
        return listMetaphor.size() + 1 + "";
    }

    public void addMetaphor(Metaphor metaphor) {
        listMetaphor.add(metaphor);
        notifyDataSetChanged();
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView name_per, name_bk, date, duration;

        public Reference(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            duration = itemView.findViewById(R.id.duration);
            name_per = itemView.findViewById(R.id.name_per);
            name_bk = itemView.findViewById(R.id.name_bk);
        }
    }
}
