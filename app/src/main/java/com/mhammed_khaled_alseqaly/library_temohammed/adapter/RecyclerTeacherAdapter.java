package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;

import java.util.ArrayList;

public class RecyclerTeacherAdapter extends RecyclerView.Adapter<RecyclerTeacherAdapter.Reference> {

    ArrayList<Teacher> listTeacher = new ArrayList<Teacher>();
    Context mContext;
    Activity activity;

    public void reListData() {
        listTeacher.clear();

        notifyDataSetChanged();
    }

    public RecyclerTeacherAdapter(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
    }

    public void addAll(ArrayList<Teacher> newTeacher) {
        int initialSize = listTeacher.size();
        this.listTeacher.addAll(newTeacher);
        notifyItemRangeInserted(initialSize, newTeacher.size());
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_std_tech, parent, false);
        return new Reference(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final Teacher teacher = listTeacher.get(position);

        holder.id.setText(teacher.getId() + "");
        holder.name.setText(teacher.getName_teacher() + "");

        holder.ic_delete_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteTeacherInDatabase(teacher,position);
            }
        });

        holder.ic_edit_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_insert_teacher);

                dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                Button btn_edit = (Button) dialog.findViewById(R.id.btn_edit_teacher);
                Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

                final EditText id_teacher = dialog.findViewById(R.id.id_teacher);
                final EditText name_teacher = dialog.findViewById(R.id.name_teacher);

                id_teacher.setText(teacher.getId() + "");
                name_teacher.setText(teacher.getName_teacher() + "");

                btn_edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Teacher newTeacher = new Teacher(
                                Integer.parseInt(id_teacher.getText().toString()),
                                name_teacher.getText().toString()
                        );

                        newTeacher.setNumber_books(teacher.getNumber_books());
                        newTeacher.setAvailable_number(teacher.getAvailable_number());
                        newTeacher.setId_row(teacher.getId_row());

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        database.getReference("database").child("teachers")
                                .child(teacher.getId_row()+"").setValue(newTeacher);

                        listTeacher.set(position , newTeacher);
                        notifyItemChanged(position);
                        dialog.dismiss();
                    }
                });

                btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

    }

    private void deleteTeacherInDatabase(final Teacher data, final int i) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child("teachers").orderByChild("id_row").equalTo(data.getId_row());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    new AlertDialog.Builder(activity)
                            .setTitle("حذف المدخلات")
                            .setMessage("هل أنت متأكد أنك تريد حذف المعلم؟?!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (dataSnapshot != null) {
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            appleSnapshot.getRef().removeValue();
                                        }
                                        Toast.makeText(mContext, "تمت عملية الحذف بنجاح ", Toast.LENGTH_SHORT).show();
                                        listTeacher.remove(i);
                                        notifyItemRemoved(i);
                                        notifyItemRangeChanged(i, listTeacher.size());
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(mContext, "لم تتم عملية الحذف بنجاح ؟!..", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listTeacher.size();
    }

    public String getSizeArray() {
        return listTeacher.size() + 1 + "";
    }

    public void addTeacher(Teacher teacher) {
        listTeacher.add(teacher);
        notifyDataSetChanged();
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView id, name;
        ImageView ic_delete_student, ic_edit_student;

        public Reference(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            name = itemView.findViewById(R.id.name);
            ic_delete_student = itemView.findViewById(R.id.ic_delete_student);
            ic_edit_student = itemView.findViewById(R.id.ic_edit_student);
        }
    }
}
