package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Metaphor;
import com.mhammed_khaled_alseqaly.library_temohammed.model.MetaphorDisplay;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Return;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;

import org.apache.poi.ss.formula.functions.T;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class RecyclerBookAdapter extends RecyclerView.Adapter<RecyclerBookAdapter.Reference> {

    Button btn_update, btn_cancel;
    EditText insert_gen_number1, insert_name_book1, insert_author1, insert_publisher1, insert_cat_book1, insert_year_publication1;
    Spinner sp_status1;
    DatabaseReference root = FirebaseDatabase.getInstance()
            .getReference("database").child("metaphors");
    DatabaseReference root_teacher = FirebaseDatabase.getInstance()
            .getReference("database").child("teachers");
    DatabaseReference root_books = FirebaseDatabase.getInstance()
            .getReference("database").child("books");
    DatabaseReference root_students = FirebaseDatabase.getInstance()
            .getReference("database").child("students");
    DatabaseReference root_metaphors_display = FirebaseDatabase.getInstance()
            .getReference("database").child("metaphors_display");
    DatabaseReference root_return_book = FirebaseDatabase.getInstance()
            .getReference("database").child("return_books");
    DatabaseReference root_no_return_book = FirebaseDatabase.getInstance()
            .getReference("database").child("no_return_books");
    ArrayList<Book> listBook;
    ArrayList<Book> newlistBook;
    ArrayList<Book> newlistBook1;

    Context mContext;
    Activity activity;

    public RecyclerBookAdapter(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
        this.listBook = new ArrayList<Book>();
        this.newlistBook = new ArrayList<Book>();
        this.newlistBook1 = new ArrayList<Book>();
    }

    public void addAll(ArrayList<Book> newBook) {
        int initialSize = listBook.size();
        int initialSize1 = newlistBook.size();
        this.listBook.addAll(newBook);
        this.newlistBook.addAll(newBook);
        notifyItemRangeInserted(initialSize, newBook.size());
        notifyItemRangeInserted(initialSize1, newBook.size());
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_book, parent, false);
        return new Reference(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final Book book = listBook.get(position);
        switch (book.getStatus()) {
            case "متاح":
                holder.view.setBackgroundResource(R.color.green);
                break;
            case "غير مرجع":
                holder.view.setBackgroundResource(R.color.yellow);
                break;
            case "مفقود":
                holder.view.setBackgroundResource(R.color.red);
                break;
            case "تالف":
                holder.view.setBackgroundResource(R.color.blue);
                break;
            case "مسحوب":
                holder.view.setBackgroundResource(R.color.zahry);
                break;
            case "لا يعار":
                holder.view.setBackgroundResource(R.color.a);
                break;
        }
        holder.name_book.setText(book.getName_book());
        holder.genNum.setText(book.getGen_number()+"");

        if (book.getAuthor_book() == null || book.getAuthor_book().equals("")) {
            holder.author_book.setText("اسم المؤلف غير موجود");
            holder.author_book.setTextColor(Color.BLUE);
        } else {
            holder.author_book.setText(book.getAuthor_book());
        }

        holder.ic_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteBookInDatabase(book, position);
            }
        });

        holder.ic_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_update);

                dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                btn_update = (Button) dialog.findViewById(R.id.btn_update);
                btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

                insert_gen_number1 = dialog.findViewById(R.id.insert_gen_number1);
                insert_name_book1 = dialog.findViewById(R.id.insert_name_book1);
                insert_author1 = dialog.findViewById(R.id.insert_author1);
                insert_publisher1 = dialog.findViewById(R.id.insert_publisher1);
                insert_cat_book1 = dialog.findViewById(R.id.insert_cat_book1);
                insert_year_publication1 = dialog.findViewById(R.id.insert_year_publication1);
                sp_status1 = dialog.findViewById(R.id.sp_status1);

                insert_gen_number1.setText(book.getGen_number() + "");
                insert_name_book1.setText(book.getName_book() + "");
                insert_author1.setText(book.getAuthor_book() + "");
                insert_publisher1.setText(book.getPublisher() + "");
                insert_year_publication1.setText(book.getYear_of_publication() + "");
                insert_cat_book1.setText(book.getCategory() + "");

                int select = 0;

                switch (book.getStatus()) {
                    case "متاح":
                        select = 0;
                        break;
                    case "غير مرجع":
                        select = 1;
                        break;
                    case "مفقود":
                        select = 2;
                        break;
                    case "تالف":
                        select = 3;
                        break;
                    case "مسحوب":
                        select = 4;
                        break;
                    case "لا يعار":
                        select = 5;
                        break;
                }
                sp_status1.setSelection(select);

                btn_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Book newBook = new Book(
                                Integer.parseInt(insert_gen_number1.getText().toString()),
                                insert_name_book1.getText().toString(),
                                insert_author1.getText().toString(),
                                insert_publisher1.getText().toString(),
                                insert_year_publication1.getText().toString()
                                , sp_status1.getSelectedItem().toString()
                        );
                        newBook.setId(book.getId());
                        newBook.setCategory(insert_cat_book1.getText().toString());

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        database.getReference("database").child("books")
                                .child(book.getId() + "").setValue(newBook);
                        listBook.set(position, newBook);
                        notifyItemChanged(position);
                        dialog.dismiss();
                    }
                });

                btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_metaphor);

                dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                final    TextView text_gen = dialog.findViewById(R.id.text_gen);

                final     TextView text_name = dialog.findViewById(R.id.text_name);
                final    Button btn_request = dialog.findViewById(R.id.btn_request);

                final      Button btn_show_person = dialog.findViewById(R.id.btn_show_person);
                final      TextView numberBook = dialog.findViewById(R.id.numberBook);
                final    TextView namePerson = dialog.findViewById(R.id.name_person);

                final EditText edit_id = dialog.findViewById(R.id.edit_id);
                final Button btn_check = dialog.findViewById(R.id.btn_check);
                final Button btn_return = dialog.findViewById(R.id.btn_return);
                final Spinner sp_person = dialog.findViewById(R.id.sp_person);

                final LinearLayout contener3 = dialog.findViewById(R.id.contenr3);
                final LinearLayout contener2_5 = dialog.findViewById(R.id.contenr2_5);
                final LinearLayout contener_f = dialog.findViewById(R.id.contener_f);
                final LinearLayout contener_ss = dialog.findViewById(R.id.contener_ss);

                text_gen.setText(book.getGen_number() + "");
                text_name.setText(book.getName_book() + "");

                btn_request.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        contener_ss.setVisibility(View.VISIBLE);
                        contener3.setVisibility(View.VISIBLE);
                        contener2_5.setVisibility(View.VISIBLE);
                        contener_f.setVisibility(View.VISIBLE);
                    }
                });

                btn_return.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (edit_id.getText() == null || edit_id.getText().toString().equals("")) {
                            Toast.makeText(mContext, "ادخل رقم الهوية اعلاه ؟!", Toast.LENGTH_SHORT).show();
                        } else {
                            deleteMetaphorInDatabase(book, Integer.parseInt(edit_id.getText().toString()),
                                    sp_person.getSelectedItem().toString(), position);
                        }
                    }
                });

                btn_show_person.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(edit_id.getText().toString().equals("") || edit_id.getText().toString() == null){
                            Toast.makeText(mContext, "ادخل رقم الهوية الشخص", Toast.LENGTH_SHORT).show();
                        }else{
                            if (sp_person.getSelectedItem().toString().equals("teachers")) {
                                root_teacher.orderByChild("id").equalTo(Integer.parseInt(edit_id.getText().toString()))
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.exists()) {

                                                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                                                        Teacher teacher = as.getValue(Teacher.class);
                                                        namePerson.setText(teacher.getName_teacher());
                                                        numberBook.setText(teacher.getNumber_books()+"");
                                                    }
                                                    listBook = newlistBook1;
                                                    notifyDataSetChanged();
                                                } else {
                                                    Toast.makeText(mContext, "لم يتم العثور على الشخص تأكد من رقم الهوية", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                            }else{
                                root_students.orderByChild("id").equalTo(Integer.parseInt(edit_id.getText().toString()))
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.exists()) {
                                                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                                                        Student student= as.getValue(Student.class);
                                                        namePerson.setText(student.getName_student());
                                                        numberBook.setText(student.getNumber_books()+"");
                                                    }
                                                    listBook = newlistBook1;
                                                    notifyDataSetChanged();
                                                } else {
                                                    Toast.makeText(mContext, "لم يتم العثور على الشخص تأكد من رقم الهوية", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                            }
                        }
                    }
                });

                btn_check.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        String str = "";
                        if (sp_person.getSelectedItem().toString().equals("معلم")) {
                            str = "teachers";
                        } else if (sp_person.getSelectedItem().toString().equals("طالب")) {
                            str = "students";
                        }
                        if (edit_id.getText() == null || edit_id.getText().toString().equals("")) {
                            Toast.makeText(mContext, "ادخل رقم الهوية اعلاه ؟!", Toast.LENGTH_SHORT).show();
                        } else {
                            if (book.getStatus().equals("لا يعار")) {
                                Toast.makeText(mContext, "هذا الكتاب لا يعار", Toast.LENGTH_SHORT).show();
                            } else {
                                borrowBook(book, Integer.parseInt(edit_id.getText().toString()), str, position);
                            }
                        }
                    }
                });


                dialog.show();
                return true;
            }
        });

    }

    private void deleteMetaphorInDatabase(final Book book, final int id, final String str, final int position) {
        root.orderByChild("id").equalTo(id)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            new AlertDialog.Builder(activity)
                                    .setTitle("حذف الاستعارة")
                                    .setMessage("هل أنت متأكد من ارجاع الكتاب؟?!")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (dataSnapshot != null) {

                                                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                                    Metaphor metaphor = appleSnapshot.getValue(Metaphor.class);
                                                    if (book.getGen_number() == metaphor.getGen_number()) {

                                                        //change student and teacher
                                                        if ("معلم".equals(str)) {
                                                            changeObjectTeacher(id, book, position,metaphor,appleSnapshot);
                                                        } else if ("طالب".equals(str)) {
                                                            changeObjectStudent(id, book, position,metaphor,appleSnapshot);
                                                        }
                                                    }
                                                }
                                            } else {
                                                Toast.makeText(mContext, "♥ تأكد من رقم المدخل او من المستعير ♥", Toast.LENGTH_SHORT).show();
                                            }
                                        }


                                    })
                                    .setNegativeButton(android.R.string.no, null)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        } else {
                            Toast.makeText(mContext, "♥ تأكد من رقم المدخل او من المستعير ♥", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(mContext, "لم تتم عملية الحذف بنجاح ؟!..", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void insertDataReturn(final Metaphor data, final String str) {
        root_return_book.orderByKey().limitToLast(1).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                        Return aReturn = new Return(data.getGen_number(), data.getName_book(), data.getName_person(), date,data.getId());
                        aReturn.setCustomize(str);

                        if (dataSnapshot.getChildrenCount() == 0) {
                            String id = "1";
                            aReturn.setId_row(id);
                            root_return_book.child(id).setValue(aReturn);
                        } else {
                            int size = 0;
                            for (DataSnapshot as : dataSnapshot.getChildren()) {
                                Return me = as.getValue(Return.class);
                                size = Integer.parseInt(me.getId_row()) + 1;
                            }
                            aReturn.setId_row(size + "");
                            root_return_book.child(size + "").setValue(aReturn);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void changeObjectStudent(int id, final Book book, final int position, final Metaphor metaphor, final DataSnapshot appleSnapshot) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child("students")
                .orderByChild("id").equalTo(id);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() >0) {
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Student student = as.getValue(Student.class);

                            //change status book
                        changeStatusBook(book.getGen_number(), position);

                        insertDataReturn(metaphor, metaphor.getCustomize());

                        int available = student.getAvailable_number() + 1;
                        student.setAvailable_number(available);

                        root_students.child(student.getId_row() + "").setValue(student);

                        appleSnapshot.getRef().removeValue();

                        Toast.makeText(mContext, "تمت عملية الارجاع بنجاح ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "تحقق من المستعير", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void changeObjectTeacher(int id, final Book book, final int position, final Metaphor metaphor, final DataSnapshot appleSnapshot) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child("teachers")
                .orderByChild("id").equalTo(id);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() >0) {
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Teacher teacher = as.getValue(Teacher.class);

                        //change status book
                        changeStatusBook(book.getGen_number(), position);

                        insertDataReturn(metaphor, metaphor.getCustomize());

                        int available = teacher.getAvailable_number() + 1;
                        teacher.setAvailable_number(available);

                        root_teacher.child(teacher.getId_row() + "").setValue(teacher);
                        appleSnapshot.getRef().removeValue();

                        Toast.makeText(mContext, "تمت عملية الارجاع بنجاح ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "تحقق من المستعير", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void changeStatusBook(int gen_number, final int position) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child("books")
                .orderByChild("gen_number").equalTo(gen_number);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot as : dataSnapshot.getChildren()) {
                    Book book = as.getValue(Book.class);
                    book.setStatus("متاح");
                    listBook.set(position, book);
                    notifyItemChanged(position);
                    root_books.child(book.getId() + "").setValue(book);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void insertMetaphorTeacher(final Metaphor metaphor, final Teacher teacher, final Book book, final int position) {
        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("metaphors")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // change teacher available .
                int available = teacher.getAvailable_number() - 1;
                int count = teacher.getNumber_books() + 1;

                teacher.setNumber_books(count);
                teacher.setAvailable_number(available);
                root_teacher.child(teacher.getId_row() + "").setValue(teacher);

                //change book status
                book.setStatus("غير مرجع");
                root_books.child(book.getId() + "").setValue(book);

                metaphor.setCustomize("معلم");
                metaphor.setCategory_book(book.getCategory());

                //insert data to meta display
                insertDataMetDisplay(metaphor, book, "معلم");

                if (dataSnapshot.getChildrenCount() == 0) {
                    String id = "1";
                    metaphor.setId_row(1);
                    root.child(id).setValue(metaphor);
                } else {
                    int size = 0;
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Metaphor me = as.getValue(Metaphor.class);
                        size = me.getId_row() + 1;
                    }
                    metaphor.setId_row(size);
                    root.child(size + "").setValue(metaphor);
                }
                listBook.set(position, book);
                notifyItemChanged(position);
                Toast.makeText(mContext, "تمت عملية الاستعارة بنجاح", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void insertDataMetDisplay(final Metaphor metaphor, final Book book, final String str_customize) {

        root_metaphors_display.orderByKey().limitToLast(1).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        MetaphorDisplay metaphorDisplay = new MetaphorDisplay(
                                metaphor.getGen_number(),
                                metaphor.getName_book(),
                                metaphor.getName_person(),
                                metaphor.getDate_metaphor(),
                                metaphor.getId()
                        );
                        metaphorDisplay.setCustomize(str_customize);
                        metaphorDisplay.setCategory_book(book.getCategory());

                        if (dataSnapshot.getChildrenCount() == 0) {
                            String id = "1";
                            metaphorDisplay.setId_row(id);
                            root_metaphors_display.child(id).setValue(metaphorDisplay);

                        } else {
                            int size = 0;
                            for (DataSnapshot as : dataSnapshot.getChildren()) {
                                MetaphorDisplay me = as.getValue(MetaphorDisplay.class);
                                size = Integer.parseInt(me.getId_row()) + 1;
                            }
                            metaphorDisplay.setId_row(size + "");
                            root_metaphors_display.child(size + "").setValue(metaphorDisplay);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }

    public void insertMetaphorStudent(final Metaphor metaphor, final Student student, final Book book, final int position) {
        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("metaphors")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // change teacher available .
                int available = student.getAvailable_number() - 1;
                int count = student.getNumber_books() + 1;

                student.setNumber_books(count);
                student.setAvailable_number(available);
                root_students.child(student.getId_row() + "").setValue(student);

                //change book status
                book.setStatus("غير مرجع");
                root_books.child(book.getId() + "").setValue(book);

                metaphor.setCustomize("طالب");
                metaphor.setCategory_book(book.getCategory());

                //insert data to meta display
                insertDataMetDisplay(metaphor, book, "طالب");


                if (dataSnapshot.getChildrenCount() == 0) {
                    String id = "1";
                    metaphor.setId_row(1);
                    root.child(id).setValue(metaphor);
                } else {
                    int size = 0;
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Metaphor me = as.getValue(Metaphor.class);
                        size = me.getId_row() + 1;
                    }
                    metaphor.setId_row(size);
                    root.child(size + "").setValue(metaphor);
                }
                listBook.set(position, book);
                notifyItemChanged(position);
                Toast.makeText(mContext, "تمت عملية الاستعارة بنجاح", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void borrowBook(final Book book, final int id_num, final String str, final int position) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child(str).orderByChild("id")
                .equalTo(id_num);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.getChildrenCount() > 0) {
                    if (str.equals("teachers")) {

                        for (DataSnapshot as : dataSnapshot.getChildren()) {
                            Teacher teacher = as.getValue(Teacher.class);

                            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                            Metaphor metaphor = new Metaphor(
                                    book.getGen_number(),
                                    teacher.getId(),
                                    book.getName_book(),
                                    teacher.getName_teacher(),
                                    date
                            );
                            if (teacher.getAvailable_number() == 0) {
                                Toast.makeText(mContext, "لثد تجاوز الحد الاقصى من استعارة الكتب.", Toast.LENGTH_SHORT).show();
                            } else {
                                insertMetaphorTeacher(metaphor, teacher, book, position);
                            }
                        }

                    } else {

                        for (DataSnapshot as : dataSnapshot.getChildren()) {
                            Student student = as.getValue(Student.class);

                            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                            Metaphor metaphor = new Metaphor(
                                    book.getGen_number(),
                                    student.getId(),
                                    book.getName_book(),
                                    student.getName_student(),
                                    date
                            );
                            if (student.getAvailable_number() == 0) {
                                Toast.makeText(mContext, "لثد تجاوز الحد الاقصى من استعارة الكتب.", Toast.LENGTH_SHORT).show();
                            } else {
                                insertMetaphorStudent(metaphor, student, book, position);
                            }
                        }
                    }
                } else {
                    Toast.makeText(activity, "♥ تأكد من المستعار او الرقم المدخل ♥", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(activity, "الرقم المدخل غير صحيح ؟!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteBookInDatabase(final Book data, final int i) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child("books").orderByChild("id").equalTo(data.getId());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    new AlertDialog.Builder(activity)
                            .setTitle("حذف المدخلات")
                            .setMessage("هل أنت متأكد أنك تريد حذف الكتاب؟?!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (dataSnapshot != null) {
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            appleSnapshot.getRef().removeValue();
                                        }
                                        Toast.makeText(mContext, "تمت عملية الحذف بنجاح ", Toast.LENGTH_SHORT).show();
                                        listBook.remove(i);
                                        notifyItemRemoved(i);
                                        notifyItemRangeChanged(i, listBook.size());

                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(mContext, "لم تتم عملية الحذف بنجاح ؟!..", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listBook.size();
    }

    public String getSizeArray() {
        return listBook.size() + 1 + "";
    }

    public void sherchFirebaseGenNumberBook(String query) {
        try {
            root_books.orderByChild("gen_number").equalTo(Integer.parseInt(query))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                listBook.clear();
                                newlistBook1.clear();
                                for (DataSnapshot as : dataSnapshot.getChildren()) {
                                    Book book = as.getValue(Book.class);
                                    newlistBook1.add(book);
                                }
                                listBook = newlistBook1;
                                notifyDataSetChanged();
                            } else {
                                Toast.makeText(mContext, "لم يتم العثور على الكتاب تأكد من رقم العام", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        } catch (Exception ex) {
        }

    }

    public void returnDataRecycler() {
        this.listBook.clear();
        this.listBook.addAll(newlistBook);
        notifyDataSetChanged();
    }

    public void reListData() {
        newlistBook.clear();
        newlistBook1.clear();
        listBook.clear();
        notifyDataSetChanged();
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView name_book, author_book,genNum;
        ImageView ic_delete, ic_edit;
        View view;

        public Reference(@NonNull View itemView) {
            super(itemView);
            genNum = itemView.findViewById(R.id.genNum);
            name_book = itemView.findViewById(R.id.name_book);
            author_book = itemView.findViewById(R.id.name_author);
            view = itemView.findViewById(R.id.view);
            ic_delete = itemView.findViewById(R.id.ic_delete);
            ic_edit = itemView.findViewById(R.id.ic_edit);
        }
    }
}
