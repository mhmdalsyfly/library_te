package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;

import java.util.ArrayList;

public class RecyclerStudentAdapter extends RecyclerView.Adapter<RecyclerStudentAdapter.Reference> {

    ArrayList<Student> listStudent = new ArrayList<Student>();
    Context mContext;
    Activity activity;


    public void reListData() {
        listStudent.clear();

        notifyDataSetChanged();
    }

    public RecyclerStudentAdapter(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
    }

    public void addAll(ArrayList<Student> newStudent) {
        int initialSize = listStudent.size();
        this.listStudent.addAll(newStudent);
        notifyItemRangeInserted(initialSize, newStudent.size());
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_std_tech, parent, false);
        return new Reference(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final Student student = listStudent.get(position);

        holder.id.setText(student.getId() + "");
        holder.name.setText(student.getName_student() + "");

        holder.ic_delete_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteStudentInDatabase(student,position);
            }
        });

        holder.ic_edit_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_insert_student);

                dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                Button btn_edit = (Button) dialog.findViewById(R.id.btn_insert_student);
                Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

                btn_edit.setText("تعديل");

                final EditText id_student = dialog.findViewById(R.id.id_student);
                final EditText name_student = dialog.findViewById(R.id.name_student);
                final EditText name_class = dialog.findViewById(R.id.name_class);

                id_student.setText(student.getId() + "");
                name_student.setText(student.getName_student() + "");
                name_class.setText(student.getName_class() + "");

                btn_edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Student newStudent = new Student(
                                Integer.parseInt(id_student.getText().toString()),
                                name_student.getText().toString(),
                                name_class.getText().toString(),
                                student.getId_row()
                        );

                        newStudent.setNumber_books(student.getNumber_books());
                        newStudent.setAvailable_number(student.getAvailable_number());

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        database.getReference("database").child("students")
                                .child(student.getId_row()+"").setValue(newStudent);

                        listStudent.set(position , newStudent);
                        notifyItemChanged(position);
                        dialog.dismiss();
                    }
                });

                btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

    }



    private void deleteStudentInDatabase(final Student data, final int i) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child("students").orderByChild("id_row").equalTo(data.getId_row());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    new AlertDialog.Builder(activity)
                            .setTitle("حذف المدخلات")
                            .setMessage("هل أنت متأكد أنك تريد حذف الطالب؟?!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (dataSnapshot != null) {
                                        for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                            appleSnapshot.getRef().removeValue();
                                        }
                                        Toast.makeText(mContext, "تمت عملية الحذف بنجاح ", Toast.LENGTH_SHORT).show();
                                        listStudent.remove(i);
                                        notifyItemRemoved(i);
                                        notifyItemRangeChanged(i, listStudent.size());
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(mContext, "لم تتم عملية الحذف بنجاح ؟!..", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listStudent.size();
    }

    public String getSizeArray() {
        return listStudent.size() - 1 + "";
    }

    public void addStudent(Student student) {
        listStudent.add(student);
        notifyDataSetChanged();
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView id, name;
        ImageView ic_delete_student, ic_edit_student;

        public Reference(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            name = itemView.findViewById(R.id.name);
            ic_delete_student = itemView.findViewById(R.id.ic_delete_student);
            ic_edit_student = itemView.findViewById(R.id.ic_edit_student);
        }
    }
}
