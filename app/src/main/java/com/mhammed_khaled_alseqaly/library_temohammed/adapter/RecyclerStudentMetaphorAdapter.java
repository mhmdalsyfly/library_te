package com.mhammed_khaled_alseqaly.library_temohammed.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;

import java.util.ArrayList;

public class RecyclerStudentMetaphorAdapter extends RecyclerView.Adapter<RecyclerStudentMetaphorAdapter.Reference> {

    ArrayList<Student> listStudent = new ArrayList<Student>();
    Context mContext;
    Activity activity;

    public RecyclerStudentMetaphorAdapter(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.activity = activity;
    }

    public void addAll(ArrayList<Student> newStudent) {
        int initialSize = listStudent.size();
        this.listStudent.addAll(newStudent);
        notifyItemRangeInserted(initialSize, newStudent.size());
    }

    @NonNull
    @Override
    public Reference onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_more_pre, parent, false);
        return new Reference(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Reference holder, final int position) {

        final Student student = listStudent.get(position);

        holder.id.setText(student.getId() + "");
        holder.name.setText(student.getName_student() + "");
        holder.mclass.setText(student.getName_class() + "");
        holder.count_book.setText("عدد الكتب : "+student.getNumber_books());

    }

    @Override
    public int getItemCount() {
        return listStudent.size();
    }

    public String getSizeArray() {
        return listStudent.size() + 1 + "";
    }

    class Reference extends RecyclerView.ViewHolder {
        TextView id, name, mclass, count_book;

        public Reference(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            name = itemView.findViewById(R.id.name);
            mclass = itemView.findViewById(R.id.mclass);
            count_book = itemView.findViewById(R.id.count_book);
        }
    }
}
