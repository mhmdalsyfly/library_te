package com.mhammed_khaled_alseqaly.library_temohammed.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerBookAdapter;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

public class KindFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView recycler;
    private ProgressBar progresBar;
    private SearchView searchView;
    private ImageView scan_parcode;
    private TextView text_data;

    private RecyclerBookAdapter adapter;

    private ArrayList<Book> newList;
    private boolean mIsLoading = false;

    private int mPostsPerPage = 30;
    private int mTotalItemCount = 0;
    private LinearLayoutManager layoutManager;

    private String mParam1;
    private String mParam2;

    public KindFragment() {
    }

    public static KindFragment newInstance(String param1, String param2) {
        KindFragment fragment = new KindFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kind, container, false);

        recycler = view.findViewById(R.id.recycler);
        progresBar = view.findViewById(R.id.progresBar);
        searchView = view.findViewById(R.id.searchView);
        scan_parcode = view.findViewById(R.id.check_parcode);
        text_data = view.findViewById(R.id.text_data);

        adapter = new RecyclerBookAdapter(getActivity(), getActivity());

        recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(layoutManager);

        recycler.setAdapter(adapter);
        returnn(mParam1, "");

        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!mIsLoading && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= mPostsPerPage) {
                    returnn(mParam1, adapter.getSizeArray() + "");
                    mIsLoading = true;
                }
            }
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.sherchFirebaseGenNumberBook(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.returnDataRecycler();
                return false;
            }
        });

        scan_parcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanCode();
            }
        });

        return view;
    }

    private void scanCode() {

        IntentIntegrator intentIntegrator = new IntentIntegrator(getActivity());
        intentIntegrator.setCaptureActivity(CaptureActivity.class);
        intentIntegrator.setOrientationLocked(false);
        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        intentIntegrator.setPrompt("Scanning Code");
        intentIntegrator.forSupportFragment(KindFragment.this).initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() != null) {

                ///AT THE END YOU NEED TO SET THE TEXT
                searchView.setQuery(result.getContents(), true);
            } else {
                Toast.makeText(getActivity(), "No Results", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public ArrayList<Book> returnn(String category, String sizeArray) {
        newList = new ArrayList<Book>();
        Query query;
        progresBar.setVisibility(View.VISIBLE);
        if (sizeArray.isEmpty())
            query = FirebaseDatabase.getInstance().getReference().child("database").child("books").child(category)
                    .orderByKey()
                    .limitToFirst(mPostsPerPage);
        else
            query = FirebaseDatabase.getInstance().getReference().child("database").child("books").child(category)
                    .orderByKey()
                    .startAt(sizeArray)
                    .limitToFirst(mPostsPerPage);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot as : dataSnapshot.getChildren()) {
                    Book book = as.getValue(Book.class);
                    newList.add(book);
                }
                adapter.addAll(newList);
                mIsLoading = false;
                progresBar.setVisibility(View.GONE);
                if (adapter.getItemCount() > 0) {
                    text_data.setVisibility(View.GONE);
                }else {
                    text_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mIsLoading = false;
            }
        });
        return newList;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPostsPerPage = 30;
        adapter.reListData();
    }

}