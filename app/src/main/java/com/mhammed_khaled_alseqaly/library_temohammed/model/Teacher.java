package com.mhammed_khaled_alseqaly.library_temohammed.model;

public class Teacher {
    int id;
    String name_teacher;
    int number_books;
    int available_number;
    int id_row;

    public Teacher() {
    }

    public Teacher(int id, String name_teacher) {
        this.id = id;
        this.name_teacher = name_teacher;
        this.number_books = 0;
        this.available_number = 5;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName_teacher() {
        return name_teacher;
    }

    public void setName_teacher(String name_teacher) {
        this.name_teacher = name_teacher;
    }

    public int getNumber_books() {
        return number_books;
    }

    public void setNumber_books(int number_books) {
        this.number_books = number_books;
    }

    public int getAvailable_number() {
        return available_number;
    }

    public void setAvailable_number(int available_number) {
        this.available_number = available_number;
    }

    public int getId_row() {
        return id_row;
    }

    public void setId_row(int id_row) {
        this.id_row = id_row;
    }
}
