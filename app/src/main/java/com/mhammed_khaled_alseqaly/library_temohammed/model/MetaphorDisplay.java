package com.mhammed_khaled_alseqaly.library_temohammed.model;

public class MetaphorDisplay {
    int gen_number;
    int id_person;
    String id_row;
    String name_book;
    String name_person;
    String date_metaphor;
    String customize;
    String category_book;

    public MetaphorDisplay() {
    }

    public MetaphorDisplay(int gen_number, String name_book, String name_person, String date_metaphor,int id_person) {
        this.gen_number = gen_number;
        this.name_book = name_book;
        this.name_person = name_person;
        this.date_metaphor = date_metaphor;
        this.id_person = id_person;
    }

    public String getId_row() {
        return id_row;
    }

    public void setId_row(String id_row) {
        this.id_row = id_row;
    }

    public String getName_book() {
        return name_book;
    }

    public void setName_book(String name_book) {
        this.name_book = name_book;
    }

    public String getName_person() {
        return name_person;
    }

    public void setName_person(String name_person) {
        this.name_person = name_person;
    }

    public String getDate_metaphor() {
        return date_metaphor;
    }

    public void setDate_metaphor(String date_metaphor) {
        this.date_metaphor = date_metaphor;
    }

    public int getGen_number() {
        return gen_number;
    }

    public void setGen_number(int gen_number) {
        this.gen_number = gen_number;
    }

    public String getCustomize() {
        return customize;
    }

    public void setCustomize(String customize) {
        this.customize = customize;
    }

    public String getCategory_book() {
        return category_book;
    }

    public void setCategory_book(String category_book) {
        this.category_book = category_book;
    }

    public int getId_person() {
        return id_person;
    }

    public void setId_person(int id_person) {
        this.id_person = id_person;
    }
}
