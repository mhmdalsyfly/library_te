package com.mhammed_khaled_alseqaly.library_temohammed.model;

public class Student {

    int id;
    String name_student;
    String name_class;
    int number_books;
    int available_number;
    int id_row;

    public Student() {
    }

    public Student(int id, String name_student, String name_class, int id_row) {
        this.id = id;
        this.name_student = name_student;
        this.name_class = name_class;
        this.number_books = 0;
        this.available_number = 3;
        this.id_row = id_row;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName_student() {
        return name_student;
    }

    public void setName_student(String name_student) {
        this.name_student = name_student;
    }

    public String getName_class() {
        return name_class;
    }

    public void setName_class(String name_class) {
        this.name_class = name_class;
    }

    public int getNumber_books() {
        return number_books;
    }

    public void setNumber_books(int number_books) {
        this.number_books = number_books;
    }

    public int getAvailable_number() {
        return available_number;
    }

    public void setAvailable_number(int available_number) {
        this.available_number = available_number;
    }

    public int getId_row() {
        return id_row;
    }

    public void setId_row(int id_row) {
        this.id_row = id_row;
    }
}
