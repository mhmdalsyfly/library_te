package com.mhammed_khaled_alseqaly.library_temohammed.model;

public class Book {
    int gen_number;
    String name_book;
    String author_book;
    String publisher;
    String year_of_publication;
    String status;
    int id;
    String category;
    public Book() {
    }

    public Book(int gen_number, String name_book, String author_book, String publisher, String year_of_publication, String status) {
        this.gen_number = gen_number;
        this.name_book = name_book;
        this.author_book = author_book;
        this.publisher = publisher;
        this.year_of_publication = year_of_publication;
        this.status = status;
    }

    public int getGen_number() {
        return gen_number;
    }

    public void setGen_number(int gen_number) {
        this.gen_number = gen_number;
    }

    public String getName_book() {
        return name_book;
    }

    public void setName_book(String name_book) {
        this.name_book = name_book;
    }

    public String getAuthor_book() {
        return author_book;
    }

    public void setAuthor_book(String author_book) {
        this.author_book = author_book;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getYear_of_publication() {
        return year_of_publication;
    }

    public void setYear_of_publication(String year_of_publication) {
        this.year_of_publication = year_of_publication;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
