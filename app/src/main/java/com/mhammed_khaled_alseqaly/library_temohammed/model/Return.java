package com.mhammed_khaled_alseqaly.library_temohammed.model;

public class Return {
    int gen_number;
    int id_person;
    String name_book;
    String name_per;
    String customize;
    String return_date;
    String id_row;


    public Return() {
    }

    public Return(int gen_number, String name_book, String name_per, String return_date,int id_person) {
        this.gen_number = gen_number;
        this.name_book = name_book;
        this.name_per = name_per;
        this.return_date = return_date;
        this.id_person = id_person;
    }

    public int getGen_number() {
        return gen_number;
    }

    public void setGen_number(int gen_number) {
        this.gen_number = gen_number;
    }

    public String getName_book() {
        return name_book;
    }

    public void setName_book(String name_book) {
        this.name_book = name_book;
    }

    public String getName_per() {
        return name_per;
    }

    public void setName_per(String name_per) {
        this.name_per = name_per;
    }

    public String getCustomize() {
        return customize;
    }

    public void setCustomize(String customize) {
        this.customize = customize;
    }

    public String getReturn_date() {
        return return_date;
    }

    public void setReturn_date(String return_date) {
        this.return_date = return_date;
    }

    public String getId_row() {
        return id_row;
    }

    public void setId_row(String id_row) {
        this.id_row = id_row;
    }

    public int getId_person() {
        return id_person;
    }

    public void setId_person(int id_person) {
        this.id_person = id_person;
    }
}
