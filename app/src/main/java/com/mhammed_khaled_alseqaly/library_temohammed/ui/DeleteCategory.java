package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Category;

import java.util.ArrayList;

public class DeleteCategory extends AppCompatActivity {

    private Button btn_delete;
    private EditText edit_cat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_category);

        initView();

        Drawable upArrow = getResources().getDrawable(R.drawable.ic_forward);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("حذف تصنيف");

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = edit_cat.getText().toString();

                if (s == null || s.equals("")) {
                    Toast.makeText(DeleteCategory.this, "اكتب التصنيف الذي تريد حذفه", Toast.LENGTH_SHORT).show();
                } else {
                    deleteCatInDatabase(s);
                }
            }
        });
    }

    private void deleteCatInDatabase(final String data) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query query = ref.child("database").child("books")
                .orderByChild("category").equalTo(data);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    new AlertDialog.Builder(DeleteCategory.this)
                            .setTitle("حذف المدخلات")
                            .setMessage("هل أنت متأكد أنك تريد حذف التصنيف؟\n علما سيتم حذف جميع الكتب الموجوده في هذا التصنيف.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                                        appleSnapshot.getRef().removeValue();
                                    }
                                    DatabaseReference nodeKeyRef = FirebaseDatabase.getInstance().getReference().child("database").child("books").child(data);
                                    nodeKeyRef.removeValue();

                                    Toast.makeText(getApplicationContext(), "تمت عملية الحذف بنجاح ", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }else {
                    Toast.makeText(DeleteCategory.this, "تأكد من اسم المدخل", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(DeleteCategory.this, "لم تتم عملية الحذف بنجاح ؟!..", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        btn_delete = findViewById(R.id.btn_delete);
        edit_cat = findViewById(R.id.edit_cat);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}