package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerStudentMetaphorAdapter;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerTeacherMetaphorAdapter;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;

import java.util.ArrayList;

public class MoreTeacherBorrowedActivity extends AppCompatActivity {

    private RecyclerView recycler_borrowed_te;
    private ProgressBar progressBar_borrowed_te;
    private TextView text_check;

    private RecyclerTeacherMetaphorAdapter adapter;

    private ArrayList<Teacher> newList;

    private boolean mIsLoading = false;
    private int mTotalItemCount = 0;
    private int mPostsPerPage = 40;

    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_teacher_borrowed);

        initView();

        adapter = new RecyclerTeacherMetaphorAdapter(getBaseContext(), MoreTeacherBorrowedActivity.this);

        recycler_borrowed_te.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(MoreTeacherBorrowedActivity.this);

        recycler_borrowed_te.setLayoutManager(layoutManager);
        recycler_borrowed_te.setAdapter(adapter);

        returnn("");

        recycler_borrowed_te.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!mIsLoading && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= mPostsPerPage) {
                    returnn(adapter.getSizeArray() + "");
                    mIsLoading = true;
                }
            }
        });

    }

    public ArrayList<Teacher> returnn(String sizeArray) {
        newList = new ArrayList<Teacher>();
        Query query;
        progressBar_borrowed_te.setVisibility(View.VISIBLE);

        query = FirebaseDatabase.getInstance().getReference().child("database").child("teachers")
                .orderByChild("number_books")
                .limitToLast(3);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot as : dataSnapshot.getChildren()) {
                    Teacher teacher = as.getValue(Teacher.class);
                    newList.add(0, teacher);
                }

                adapter.addAll(newList);
                mIsLoading = false;
                progressBar_borrowed_te.setVisibility(View.GONE);

                if (adapter.getItemCount()>0) {
                    text_check.setVisibility(View.GONE);
                }else{
                    text_check.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mIsLoading = false;
            }
        });
        return newList;
    }

    private void initView() {
        recycler_borrowed_te = findViewById(R.id.recycler_more);
        progressBar_borrowed_te = findViewById(R.id.progressBar_more);
        text_check = findViewById(R.id.text_check);

        Drawable upArrow = getResources().getDrawable(R.drawable.ic_forward);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("اكثر ثلاث معلمون استعارة");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}