package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerMetaAdapter;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerStudentAdapter;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Metaphor;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;

import java.util.ArrayList;

public class MetaphorsActivity extends AppCompatActivity {

    private RecyclerView recycler_met;
    private ProgressBar progressBar_met;
    private TextView text_check;

    private DatabaseReference root;

    private RecyclerMetaAdapter adapter;

    private ArrayList<Metaphor> newList;
    private boolean mIsLoading = false;
    private int mTotalItemCount = 0;
    private int mPostsPerPage = 100;

    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metaphors);

        initView();

        adapter = new RecyclerMetaAdapter(getBaseContext(), MetaphorsActivity.this);

        recycler_met.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(MetaphorsActivity.this);

        recycler_met.setLayoutManager(layoutManager);
        recycler_met.setAdapter(adapter);

        returnn("");

        recycler_met.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!mIsLoading && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= mPostsPerPage) {
                    returnn(adapter.getSizeArray() + "");
                    mIsLoading = true;
                }
            }
        });
    }

    public ArrayList<Metaphor> returnn(String sizeArray) {
        newList = new ArrayList<Metaphor>();
        Query query;
        progressBar_met.setVisibility(View.VISIBLE);
        if (sizeArray.isEmpty())
            query = FirebaseDatabase.getInstance().getReference().child("database").child("metaphors")
                    .orderByKey()
                    .limitToFirst(mPostsPerPage);
        else
            query = FirebaseDatabase.getInstance().getReference().child("database").child("metaphors")
                    .orderByKey()
                    .startAt(sizeArray)
                    .limitToFirst(mPostsPerPage);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot as : dataSnapshot.getChildren()) {
                    Metaphor metaphor = as.getValue(Metaphor.class);
                    newList.add(metaphor);
                }
                adapter.addAll(newList);
                mIsLoading = false;
                progressBar_met.setVisibility(View.GONE);

                if (adapter.getItemCount()>0) {
                    text_check.setVisibility(View.GONE);
                }else{
                    text_check.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mIsLoading = false;
            }
        });
        return newList;
    }


    private void initView() {
        recycler_met = findViewById(R.id.recycler_met);
        progressBar_met = findViewById(R.id.progressBar_met);
        text_check = findViewById(R.id.text_check);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        root = database.getReference("database").child("metaphors");

        Drawable upArrow = getResources().getDrawable(R.drawable.ic_forward);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("الاستعارات");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}