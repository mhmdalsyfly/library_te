package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerTeacherAdapter;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;
import com.obsez.android.lib.filechooser.ChooserDialog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class TeacherActivity extends AppCompatActivity {

    private RecyclerView recycler_teacher;
    private ProgressBar progressBar_teacher;
    private FloatingActionButton fab_insert, import_to_app;
    private DatabaseReference root;

    private RecyclerTeacherAdapter adapter;

    private TextView text_check;

    private ArrayList<Teacher> newList;
    private boolean mIsLoading = false;

    private LinearLayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher);

        initView();
        adapter = new RecyclerTeacherAdapter(getBaseContext(), TeacherActivity.this);

        recycler_teacher.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(TeacherActivity.this);

        recycler_teacher.setLayoutManager(layoutManager);
        recycler_teacher.setAdapter(adapter);

        returnn("");


        import_to_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ChooserDialog().with(TeacherActivity.this)
                        .withStartFile("/storage/emulated/0/")
                        .withChosenListener(new ChooserDialog.Result() {
                            @Override
                            public void onChoosePath(String path, File pathFile) {
                                try {
                                    if (path.endsWith(".xlsx")) {
                                        readFileXLSX(pathFile);
                                    } else {
                                        readFileXLS(pathFile);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (InvalidFormatException e) {
                                    e.printStackTrace();
                                }

                            }
                        })
                        .build()
                        .show();
            }
        });

        fab_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                {
                    final Dialog dialog = new Dialog(TeacherActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_insert_teacher);

                    dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    final EditText id_teacher = dialog.findViewById(R.id.id_teacher);
                    final EditText name_teacher = dialog.findViewById(R.id.name_teacher);

                    Button btn_cancel = dialog.findViewById(R.id.btn_cancel);
                    Button btn_insert = dialog.findViewById(R.id.btn_edit_teacher);

                    btn_insert.setText("اضافة");

                    btn_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    btn_insert.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int id = Integer.parseInt(id_teacher.getText().toString());
                            String name = name_teacher.getText().toString();

                            if (name == null || name.equals("") || id_teacher.getText() == null || id_teacher.equals("")) {
                                Toast.makeText(TeacherActivity.this, "تأكد من تعبئة الحقول", Toast.LENGTH_SHORT).show();
                            } else {
                                insertTeacherDatabase(id, name, getBaseContext());
                            }
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            }
        });

    }


    public ArrayList<Teacher> returnn(String sizeArray) {
        newList = new ArrayList<Teacher>();
        progressBar_teacher.setVisibility(View.VISIBLE);

        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("teachers")
                .orderByKey();


        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot as : dataSnapshot.getChildren()) {
                    Teacher teacher = as.getValue(Teacher.class);
                    newList.add(teacher);
                }
                adapter.addAll(newList);
                mIsLoading = false;
                progressBar_teacher.setVisibility(View.GONE);

                if (adapter.getItemCount() > 0) {
                    text_check.setVisibility(View.GONE);
                } else {
                    text_check.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mIsLoading = false;
            }
        });
        return newList;
    }

    private void insertTeacherDatabase(final int id, final String name, final Context mContext) {
        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("teachers")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                text_check.setVisibility(View.GONE);

                FirebaseDatabase.getInstance().getReference().child("database").child("teachers")
                        .orderByChild("id").equalTo(id)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                                if (dataSnapshot2.exists()) {
                                    Toast.makeText(mContext, "هذا المعلم لديه نفس الرقم الهوية المسجل في قاعدة بياناتنا", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (dataSnapshot.getChildrenCount() == 0) {

                                        Teacher teacher = new Teacher(id, name);

                                        teacher.setNumber_books(0);
                                        teacher.setAvailable_number(5);
                                        teacher.setId_row(1);
                                        String id_row = "1";

                                        root.child(id_row).setValue(teacher);
                                        adapter.addTeacher(teacher);
                                    } else {
                                        int size = 0;
                                        for (DataSnapshot as : dataSnapshot.getChildren()) {
                                            Teacher te = as.getValue(Teacher.class);
                                            size = te.getId_row() + 1;
                                        }
                                        Teacher teacher = new Teacher(id, name);
                                        teacher.setNumber_books(0);
                                        teacher.setAvailable_number(5);
                                        teacher.setId_row(size);

                                        root.child(size + "").setValue(teacher);
                                        adapter.addTeacher(teacher);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void initView() {
        Drawable upArrow = getResources().getDrawable(R.drawable.ic_forward);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("المعلمون");

        recycler_teacher = findViewById(R.id.recycler_teacher);
        progressBar_teacher = findViewById(R.id.progressBar_teacher);
        fab_insert = findViewById(R.id.fab_add);
        text_check = findViewById(R.id.text_check);
        import_to_app = findViewById(R.id.import_to_app);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        root = database.getReference("database").child("teachers");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void readFileXLS(final File pathFile) throws IOException, InvalidFormatException {
        mIsLoading = true;
        progressBar_teacher.setVisibility(View.VISIBLE);
        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("teachers")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int indexRow = 0;
                if (dataSnapshot.getChildrenCount() == 0) {
                    indexRow = 0;
                } else {
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Student st = as.getValue(Student.class);
                        indexRow = st.getId_row();
                    }

                }
                Workbook wb = null;
                try {
                    wb = WorkbookFactory.create(new FileInputStream(pathFile));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvalidFormatException e) {
                    e.printStackTrace();
                }
                Sheet mySheet = wb.getSheetAt(0);

                System.out.println(mySheet.getRow(1).getCell(0));

                Iterator<Row> rowIter = mySheet.rowIterator();

                int rowno = 0;
                while (rowIter.hasNext()) {
                    HSSFRow myRow = (HSSFRow) rowIter.next();

                    if (rowno != 0) {
                        int id = 0;
                        String name_teacher = null;

                        Iterator<Cell> cellIter = myRow.cellIterator();

                        while (cellIter.hasNext()) {
                            HSSFCell myCell = (HSSFCell) cellIter.next();

                            Log.e("22TAG", " Index :" + myCell.getColumnIndex() + " -- " + myCell.toString());

                            switch (myCell.getColumnIndex()) {
                                case 0:
                                    id = (int) Float.parseFloat(myCell.toString());
                                    break;
                                case 1:
                                    name_teacher = myCell.toString();

                                    break;
                            }
                        }


                        Teacher teacher = new Teacher(id, name_teacher);
                        teacher.setId_row(indexRow);
                        root.child(indexRow + "").setValue(teacher);

                    }

                    indexRow++;
                    rowno++;
                }

                progressBar_teacher.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn("");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressBar_teacher.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn("");
            }
        });


    }

    private void readFileXLSX(final File filename) throws IOException, InvalidFormatException {

        mIsLoading = true;
        progressBar_teacher.setVisibility(View.VISIBLE);

        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("teachers")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int indexRow = 0;
                if (dataSnapshot.getChildrenCount() == 0) {
                    indexRow = 0;
                } else {
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Student st = as.getValue(Student.class);
                        indexRow = st.getId_row();
                    }

                }
                XSSFWorkbook wb = null;
                try {
                    wb = new XSSFWorkbook(new FileInputStream(filename));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Sheet mySheet = wb.getSheetAt(0);
                Iterator<Row> rowIterator = mySheet.iterator();
                int rowno = 0;
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    if (rowno != 0) {
                        int id = 0;
                        String name_teacher = null;
                        Iterator<Cell> cellIterator = row.cellIterator();
                        int colno = 0;
                        while (cellIterator.hasNext()) {
                            Cell cell = cellIterator.next();
                            colno++;
                            Log.e("22TAG", " Index :" + cell.getColumnIndex() + " -- " + cell.toString());
                            switch (cell.getColumnIndex()) {
                                case 0:
                                    id = (int) Float.parseFloat(cell.toString());
                                    break;
                                case 1:
                                    name_teacher = cell.toString();

                                    break;

                            }
                        }
                        Teacher teacher = new Teacher(id, name_teacher);
                        teacher.setId_row(indexRow);
                        root.child(indexRow + "").setValue(teacher);
                    }
                    rowno++;
                    indexRow++;
                }

                progressBar_teacher.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn("");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressBar_teacher.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn("");
            }
        });


    }


}