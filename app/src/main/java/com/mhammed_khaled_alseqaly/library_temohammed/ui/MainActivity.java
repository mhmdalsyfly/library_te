package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerBookAdapter;
import com.mhammed_khaled_alseqaly.library_temohammed.fragment.CaptureActivity;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;
import com.obsez.android.lib.filechooser.ChooserDialog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final float END_SCALE = 0.7f;

    private SwipeRefreshLayout swipeRefresh;
    private CardView cardView;
    private ImageView img_menu;
    private DrawerLayout drawer;
    private CoordinatorLayout coordinator;
    private NavigationView nav_view;

    private RecyclerView recycler;
    private ProgressBar progresBar;
    private SearchView searchView;
    private ImageView scan_parcode, import_to_app;
    private TextView text_data;

    private RecyclerBookAdapter adapter;

    private ArrayList<Book> newList;
    private boolean mIsLoading = false;


    private LinearLayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        adapter = new RecyclerBookAdapter(getBaseContext(), MainActivity.this);

        recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(MainActivity.this);
        recycler.setLayoutManager(layoutManager);

        recycler.setAdapter(adapter);
        returnn();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.sherchFirebaseGenNumberBook(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.returnDataRecycler();
                return false;
            }
        });

        import_to_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ChooserDialog().with(MainActivity.this)
                        .withStartFile("/storage/emulated/0/")
                        .withChosenListener(new ChooserDialog.Result() {
                            @Override
                            public void onChoosePath(String path, File pathFile) {
                                try {
                                    if (path.endsWith(".xlsx")) {
                                        readFileXLSX(pathFile);
                                    } else {
                                        readFileXLS(pathFile);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (InvalidFormatException e) {
                                    e.printStackTrace();
                                }

                            }
                        })
                        .build()
                        .show();
            }
        });


        scan_parcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanCode();
            }
        });


        img_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        animateNavigationDrawer();

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete_category:
                        Intent intent1 = new Intent(MainActivity.this, DeleteCategory.class);
                        startActivity(intent1);
                        break;
                    case R.id.insert_bok:
                        Intent intent2 = new Intent(MainActivity.this, InsertBook.class);
                        startActivity(intent2);
                        break;
                    case R.id.students:
                        Intent intent3 = new Intent(MainActivity.this, StudentActivity.class);
                        startActivity(intent3);
                        break;
                    case R.id.teachers:
                        Intent intent4 = new Intent(MainActivity.this, TeacherActivity.class);
                        startActivity(intent4);
                        break;
                    case R.id.metaphor:
                        Intent intent5 = new Intent(MainActivity.this, MetaphorsActivity.class);
                        startActivity(intent5);
                        break;
                    case R.id.book_losts:
                        Intent intent6 = new Intent(MainActivity.this, LoseBookActivity.class);
                        startActivity(intent6);
                        break;
                    case R.id.returned_book:
                        Intent intent7 = new Intent(MainActivity.this, ReturnBookActivity.class);
                        startActivity(intent7);
                        break;
                    case R.id.non_return_book:
                        Intent intent8 = new Intent(MainActivity.this, NonReturnBookActivity.class);
                        startActivity(intent8);
                        break;
                    case R.id.book_metaphor:
                        Intent intent9 = new Intent(MainActivity.this, BorrowedBooksActivity.class);
                        startActivity(intent9);
                        break;
                    case R.id.most_students_metaphors:
                        Intent intent10 = new Intent(MainActivity.this, MoreStudentBorrowedActivity.class);
                        startActivity(intent10);
                        break;
                    case R.id.most_teacher_metaphors:
                        Intent intent11 = new Intent(MainActivity.this, MoreTeacherBorrowedActivity.class);
                        startActivity(intent11);
                        break;
                }
                drawer.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    private void toggleRefreshing(boolean b) {
        if (swipeRefresh != null) {
            swipeRefresh.setEnabled(b);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerVisible(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else
            super.onBackPressed();
    }


    private void animateNavigationDrawer() {
        drawer.setScrimColor(getResources().getColor(R.color.green));
        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                final float offsetScale = 1 - diffScaledOffset;
                coordinator.setScaleX(offsetScale);
                coordinator.setScaleY(offsetScale);

                final float xOffset = drawerView.getWidth() * slideOffset;
                final float xOffsetDiff = coordinator.getWidth() * diffScaledOffset / 2;
                float xTranslation = 0;
                SharedPreferences sharedPreferences = getSharedPreferences("language_app", MODE_PRIVATE);
                String language = sharedPreferences.getString("KEY", Locale.getDefault().getLanguage());

                if (language.equals("ar")) {
                    drawer.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                    xTranslation = xOffsetDiff - xOffset;
                } else {
                    drawer.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                    xTranslation = xOffset - xOffsetDiff;
                }

                coordinator.setTranslationX(xTranslation);
            }
        });
    }

    private void readFileXLS(final File pathFile) throws IOException, InvalidFormatException {
        mIsLoading = true;
        progresBar.setVisibility(View.VISIBLE);
        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("books")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int id = 0;
                if (dataSnapshot.getChildrenCount() == 0) {
                    id = 0;
                } else {
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Student st = as.getValue(Student.class);
                        id = st.getId_row();
                    }

                }
                Workbook wb = null;
                try {
                    wb = WorkbookFactory.create(new FileInputStream(pathFile));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvalidFormatException e) {
                    e.printStackTrace();
                }
                Sheet mySheet = wb.getSheetAt(0);

                System.out.println(mySheet.getRow(1).getCell(0));

                Iterator<Row> rowIter = mySheet.rowIterator();

                int rowno = 0;
                while (rowIter.hasNext()) {
                    HSSFRow myRow = (HSSFRow) rowIter.next();

                    if (rowno != 0) {

                        int gen_number = 0;

                        String author_book = null, year_of_publication = null, status = null, publisher = null, name_book = null, category = null;

                        Iterator<Cell> cellIter = myRow.cellIterator();

                        while (cellIter.hasNext()) {
                            HSSFCell myCell = (HSSFCell) cellIter.next();

                            Log.e("22TAG", " Index :" + myCell.getColumnIndex() + " -- " + myCell.toString());

                            switch (myCell.getColumnIndex()) {
                                case 0:
                                    gen_number = (int) Float.parseFloat(myCell.toString());
                                    break;
                                case 1:
                                    name_book = myCell.toString();
                                    break;
                                case 2:
                                    author_book = myCell.toString();
                                    break;
                                case 3:
                                    publisher = myCell.toString();
                                    break;
                                case 4:
                                    year_of_publication = myCell.toString();
                                    break;
                                case 5:
                                    category = myCell.toString();
                                    break;
                                case 6:
                                    status = myCell.toString();
                                    break;
                            }
                        }

                        Book book = new Book(gen_number, name_book, author_book, publisher, year_of_publication, status);
                        book.setId(id);
                        book.setCategory(category);
                        FirebaseDatabase.getInstance().getReference().child("database")
                                .child("books").child(id + "").setValue(book);

                    }

                    id++;
                    rowno++;
                }

                progresBar.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progresBar.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn();
            }
        });


    }

    private void readFileXLSX(final File filename) throws IOException, InvalidFormatException {

        mIsLoading = true;
        progresBar.setVisibility(View.VISIBLE);

        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("books")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int id = 0;
                if (dataSnapshot.getChildrenCount() == 0) {
                    id = 0;
                } else {
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Student st = as.getValue(Student.class);
                        id = st.getId();
                    }

                }
                System.out.println("asdfasdfasdfasdfffffff"+id);
                XSSFWorkbook wb = null;
                try {
                    wb = new XSSFWorkbook(new FileInputStream(filename));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Sheet mySheet = wb.getSheetAt(0);
                Iterator<Row> rowIterator = mySheet.iterator();
                int rowno = 0;
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    if (rowno != 0) {

                        int gen_number = 0;

                        String author_book = null, year_of_publication = null, status = null, publisher = null, name_book = null, category = null;

                        Iterator<Cell> cellIterator = row.cellIterator();
                        int colno = 0;
                        while (cellIterator.hasNext()) {
                            Cell cell = cellIterator.next();
                            colno++;
                            Log.e("22TAG", " Index :" + cell.getColumnIndex() + " -- " + cell.toString());

                            switch (cell.getColumnIndex()) {
                                case 0:
                                    gen_number = (int) Float.parseFloat(cell.toString());
                                    break;
                                case 1:
                                    name_book = cell.toString();
                                    break;
                                case 2:
                                    author_book = cell.toString();
                                    break;
                                case 3:
                                    publisher = cell.toString();
                                    break;
                                case 4:
                                    year_of_publication = cell.toString();
                                    break;
                                case 5:
                                    category = cell.toString();
                                    break;
                                case 6:
                                    status = cell.toString();
                                    break;
                            }


                        }
                        Book book = new Book(gen_number, name_book, author_book, publisher, year_of_publication, status);
                        book.setId(id);
                        book.setCategory(category);
                        FirebaseDatabase.getInstance().getReference().child("database")
                                .child("books").child(id + "").setValue(book);
                    }
                    rowno++;
                    id++;
                }

                progresBar.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progresBar.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn();
            }
        });


    }


    private void initView() {
        swipeRefresh = findViewById(R.id.swipeRefresh);
        cardView = findViewById(R.id.cardView);
        cardView.setBackgroundResource(R.drawable.bg_card_view);
        img_menu = findViewById(R.id.img_menu);
        drawer = findViewById(R.id.drawer_layout);
        coordinator = findViewById(R.id.coordinator);
        nav_view = findViewById(R.id.nav_view);
        recycler = findViewById(R.id.recycler);
        progresBar = findViewById(R.id.progresBar);
        searchView = findViewById(R.id.searchView);
        scan_parcode = findViewById(R.id.check_parcode);
        text_data = findViewById(R.id.text_data);
        import_to_app = findViewById(R.id.import_to_app);

        TextView name_admin = nav_view.getHeaderView(0).findViewById(R.id.name_admin);
        TextView name_app = nav_view.getHeaderView(0).findViewById(R.id.name_app);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "Omar-Regular.ttf");

        name_admin.setTypeface(face);
        name_app.setTypeface(face);


        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.white));
        swipeRefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.green));
        swipeRefresh.setOnRefreshListener(this);
    }

    private void scanCode() {
        IntentIntegrator intentIntegrator = new IntentIntegrator(MainActivity.this);
        intentIntegrator.setCaptureActivity(CaptureActivity.class);
        intentIntegrator.setOrientationLocked(false);
        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        intentIntegrator.setPrompt("Scanning Code");
        intentIntegrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() != null) {

                ///AT THE END YOU NEED TO SET THE TEXT
                searchView.setQuery(result.getContents(), true);
            } else {
                Toast.makeText(MainActivity.this, "No Results", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public ArrayList<Book> returnn() {
        newList = new ArrayList<Book>();
        progresBar.setVisibility(View.VISIBLE);
        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("books")
                .orderByKey();


        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                for (DataSnapshot as : dataSnapshot.getChildren()) {
                    Book book = as.getValue(Book.class);
                    newList.add(book);
                }
                adapter.addAll(newList);
                mIsLoading = false;
                progresBar.setVisibility(View.GONE);
                if (adapter.getItemCount() > 0) {
                    text_data.setVisibility(View.GONE);
                } else {
                    text_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mIsLoading = false;
            }
        });

        return newList;
    }


    @Override
    public void onRefresh() {

        adapter.reListData();
        returnn();
        swipeRefresh.setRefreshing(false);
    }
}