package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Category;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Teacher;

import java.util.ArrayList;

public class InsertBook extends AppCompatActivity {

    private EditText insert_gen_number, insert_name_book, insert_author,
            insert_publisher, insert_year_publication,insert_cat_book;
    private Spinner sp_status;
    private Button btn_insert;
    private DatabaseReference root;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_book);

        initView();

        btn_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final    String gen = insert_gen_number.getText().toString();
                final    String nameBook = insert_name_book.getText().toString();
                final    String author = insert_author.getText().toString();
                final    String yearPub = insert_year_publication.getText().toString();
                final    String pub = insert_publisher.getText().toString();
                final   String status = sp_status.getSelectedItem().toString();
                final  String category = insert_cat_book.getText().toString();

                if (gen == null || gen.equals("") || nameBook == null || nameBook.equals("")||
                category == null || category.equals("") ) {
                    Toast.makeText(InsertBook.this, "تأكد من ادخال رقم العام و اسم الكتاب و التصنيف", Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseDatabase.getInstance().getReference().child("database").child("books")
                            .orderByChild("gen_number").equalTo(Integer.parseInt(insert_gen_number.getText().toString().trim()))
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                                    if (dataSnapshot2.exists()) {
                                        Toast.makeText(getBaseContext(), "هذا الكتاب لديه نفس الرقم العام المسجل في قاعدة بياناتنا", Toast.LENGTH_SHORT).show();
                                    } else {
                                        insertBookDatabase(category, new Book(Integer.parseInt(gen), nameBook, author, pub, yearPub, status));
                                        Toast.makeText(InsertBook.this, "تمت عملية الادخال بنجاح", Toast.LENGTH_SHORT).show();
                                        insert_gen_number.getText().clear();
                                        insert_name_book.getText().clear();
                                        insert_author.getText().clear();
                                        insert_year_publication.getText().clear();
                                        insert_publisher.getText().clear();
                                        insert_cat_book.getText().clear();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                }
            }
        });
    }

    private void insertBookDatabase(final String category,final Book book) {
        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("books")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0){
                    String id = "1";
                    book.setId(1);
                    book.setCategory(category);
                    root.child(id).setValue(book);
                }else {
                    int size = 0;
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Book book = as.getValue(Book.class);
                        size = book.getId() + 1;
                    }
                    book.setId(size);
                    book.setCategory(category);
                    root.child(size+"").setValue(book);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void initView() {
        insert_gen_number = findViewById(R.id.insert_gen_number);
        insert_name_book = findViewById(R.id.insert_name_book);
        insert_author = findViewById(R.id.insert_author);
        insert_publisher = findViewById(R.id.insert_publisher);
        insert_year_publication = findViewById(R.id.insert_year_publication);
        insert_cat_book = findViewById(R.id.insert_cat_book);
        sp_status = findViewById(R.id.sp_status);
        btn_insert = findViewById(R.id.btn_insert);

        Drawable upArrow = getResources().getDrawable(R.drawable.ic_forward);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("اضافة كتاب");

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        root = database.getReference("database").child("books");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}