package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerMetaDisplayAdapter2;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerStudentMetaphorAdapter;
import com.mhammed_khaled_alseqaly.library_temohammed.model.MetaphorDisplay;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;

import java.util.ArrayList;

public class MoreStudentBorrowedActivity extends AppCompatActivity {

    private RecyclerView recycler_borrowed_st;
    private ProgressBar progressBar_borrowed_st;
    private TextView text_check;

    private RecyclerStudentMetaphorAdapter adapter;

    private ArrayList<Student> newList;

    private boolean mIsLoading = false;
    private int mTotalItemCount = 0;
    private int mPostsPerPage = 40;

    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_student_borrowed);

        initView();

        adapter = new RecyclerStudentMetaphorAdapter(getBaseContext(), MoreStudentBorrowedActivity.this);

        recycler_borrowed_st.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(MoreStudentBorrowedActivity.this);

        recycler_borrowed_st.setLayoutManager(layoutManager);
        recycler_borrowed_st.setAdapter(adapter);

        returnn("");

        recycler_borrowed_st.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!mIsLoading && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= mPostsPerPage) {
                    returnn(adapter.getSizeArray() + "");
                    mIsLoading = true;
                }
            }
        });

    }

    public ArrayList<Student> returnn(String sizeArray) {
        newList = new ArrayList<Student>();
        Query query;
        progressBar_borrowed_st.setVisibility(View.VISIBLE);
        query = FirebaseDatabase.getInstance().getReference().child("database").child("students")
                .orderByChild("number_books")
                .limitToLast(3);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot as : dataSnapshot.getChildren()) {
                    Student student = as.getValue(Student.class);
                    newList.add(0,student);
                }

                adapter.addAll(newList);
                mIsLoading = false;
                progressBar_borrowed_st.setVisibility(View.GONE);

                if (adapter.getItemCount()>0) {
                    text_check.setVisibility(View.GONE);
                }else{
                    text_check.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mIsLoading = false;
            }
        });
        return newList;
    }

    private void initView() {
        recycler_borrowed_st = findViewById(R.id.recycler_more);
        progressBar_borrowed_st = findViewById(R.id.progressBar_more);
        text_check = findViewById(R.id.text_check);

        Drawable upArrow = getResources().getDrawable(R.drawable.ic_forward);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("اكثر ثلاث طلاب استعارة");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}