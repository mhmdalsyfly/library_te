package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerMetaDisplayAdapter2;
import com.mhammed_khaled_alseqaly.library_temohammed.model.MetaphorDisplay;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class BorrowedBooksActivity extends AppCompatActivity {
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private RecyclerView recycler_borrowed;
    private ProgressBar progressBar_borrowed;

    private RecyclerMetaDisplayAdapter2 adapter;

    private Button btn_export;
    private TextView count_book;
    private TextView text_check;

    private ArrayList<MetaphorDisplay> newList;

    private boolean mIsLoading = false;
    private int mTotalItemCount = 0;
    private int mPostsPerPage = 60;

    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrowed_books);

        initView();

        adapter = new RecyclerMetaDisplayAdapter2(getBaseContext(), BorrowedBooksActivity.this);

        recycler_borrowed.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(BorrowedBooksActivity.this);

        recycler_borrowed.setLayoutManager(layoutManager);
        recycler_borrowed.setAdapter(adapter);

        returnn("");

        recycler_borrowed.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!mIsLoading && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= mPostsPerPage) {
                    returnn(adapter.getSizeArray() + "");
                    mIsLoading = true;
                }
            }
        });

        btn_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyStoragePermissions(BorrowedBooksActivity.this);
                exportToExcel();
            }
        });

    }

    private void exportToExcel() {
        final Dialog dialog = new Dialog(BorrowedBooksActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_name_file);

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        Button btn_ex = dialog.findViewById(R.id.btn_ex);
        Button btn_cancel = dialog.findViewById(R.id.btn_cancel);
        final EditText file_name = dialog.findViewById(R.id.name_file);

        btn_ex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (file_name.getText() == null || file_name.getText().toString().equals("")) {
                    Toast.makeText(BorrowedBooksActivity.this, "رجاء ادخل اسم الملف لحفظه على الجهاز", Toast.LENGTH_SHORT).show();
                } else {
                    saveToFile(file_name.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void saveToFile(String toString) {
        Workbook workbook = new HSSFWorkbook();
        Cell cell = null;
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);

        Sheet sheet = null;
        sheet = workbook.createSheet("Export Data");

        Row row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("رقم العام");
        cell.setCellStyle(cellStyle);

        cell = row.createCell(1);
        cell.setCellValue("اسم المستعير");
        cell.setCellStyle(cellStyle);

        cell = row.createCell(2);
        cell.setCellValue("التصنيف");
        cell.setCellStyle(cellStyle);

        cell = row.createCell(3);
        cell.setCellValue("اسم الكتاب");
        cell.setCellStyle(cellStyle);

        cell = row.createCell(4);
        cell.setCellValue("تاريخ الاستعارة");
        cell.setCellStyle(cellStyle);


        sheet.setColumnWidth(0, (350 * 10));
        sheet.setColumnWidth(1, (350 * 10));
        sheet.setColumnWidth(2, (350 * 10));
        sheet.setColumnWidth(3, (350 * 10));
        sheet.setColumnWidth(4, (350 * 10));

        int i = 1;
        for (MetaphorDisplay aReturn : adapter.getAllElements()) {
            row = sheet.createRow(i);

            cell = row.createCell(0);
            cell.setCellValue(aReturn.getGen_number());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(1);
            cell.setCellValue(aReturn.getName_person());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(2);
            cell.setCellValue(aReturn.getCustomize());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(3);
            cell.setCellValue(aReturn.getName_book());
            cell.setCellStyle(cellStyle);

            cell = row.createCell(4);
            cell.setCellValue(aReturn.getDate_metaphor());
            cell.setCellStyle(cellStyle);

            sheet.setColumnWidth(0, (350 * 10));
            sheet.setColumnWidth(1, (350 * 10));
            sheet.setColumnWidth(2, (350 * 10));
            sheet.setColumnWidth(3, (350 * 10));
            sheet.setColumnWidth(4, (350 * 10));
            i++;
        }

        File parent_file = new File(Environment.getExternalStorageDirectory() + "/excel");
        // have the object build the directory structure, if needed.
        if (!parent_file.exists()) {
            parent_file.mkdirs();
        }

        File path_file = new File(parent_file + File.separator + toString + ".xls");
        String path = path_file.getAbsolutePath();
        File file = new File(path);
        try {
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            Toast.makeText(BorrowedBooksActivity.this, "Export successful", Toast.LENGTH_SHORT).show();
            out.flush();
            out.close();

        } catch (Exception e) {
            Log.d("1tag", e.getMessage() + "");
            Toast.makeText(BorrowedBooksActivity.this, "Export no successful", Toast.LENGTH_SHORT).show();
        }
    }

    public ArrayList<MetaphorDisplay> returnn(String sizeArray) {
        newList = new ArrayList<MetaphorDisplay>();
        Query query;
        progressBar_borrowed.setVisibility(View.VISIBLE);
        if (sizeArray.isEmpty())
            query = FirebaseDatabase.getInstance().getReference().child("database").child("metaphors_display")
                    .orderByKey()
                    .limitToFirst(mPostsPerPage);
        else
            query = FirebaseDatabase.getInstance().getReference().child("database").child("metaphors_display")
                    .orderByKey()
                    .startAt(sizeArray)
                    .limitToFirst(mPostsPerPage);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot as : dataSnapshot.getChildren()) {
                    MetaphorDisplay metaphorDisplay = as.getValue(MetaphorDisplay.class);
                    newList.add(metaphorDisplay);
                }
                adapter.addAll(newList);
                mIsLoading = false;
                progressBar_borrowed.setVisibility(View.GONE);
                count_book.setText("عدد الكتب : " + adapter.getItemCount());

                if (adapter.getItemCount() > 0) {
                    text_check.setVisibility(View.GONE);
                } else {
                    text_check.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mIsLoading = false;
            }
        });
        return newList;
    }

    private void initView() {
        recycler_borrowed = findViewById(R.id.recycler_borrowed);
        progressBar_borrowed = findViewById(R.id.progressBar_borrowed);
        count_book = findViewById(R.id.count_book);
        btn_export = findViewById(R.id.btn_export);
        text_check = findViewById(R.id.text_check);

        Drawable upArrow = getResources().getDrawable(R.drawable.ic_forward);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("الكتب المستعارة");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

}