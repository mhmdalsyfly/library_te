package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mhammed_khaled_alseqaly.library_temohammed.R;
import com.mhammed_khaled_alseqaly.library_temohammed.adapter.RecyclerStudentAdapter;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Book;
import com.mhammed_khaled_alseqaly.library_temohammed.model.Student;
import com.obsez.android.lib.filechooser.ChooserDialog;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class StudentActivity extends AppCompatActivity {

    private RecyclerView recycler_student;
    private ProgressBar progressBar_student;
    private FloatingActionButton fab_insert, import_to_app;
    private DatabaseReference root;

    private RecyclerStudentAdapter adapter;

    private ArrayList<Student> newList;
    private boolean mIsLoading = false;

    private TextView text_check;

    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        initView();

        adapter = new RecyclerStudentAdapter(getBaseContext(), StudentActivity.this);

        recycler_student.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(StudentActivity.this);

        recycler_student.setLayoutManager(layoutManager);
        recycler_student.setAdapter(adapter);

        returnn("");

        import_to_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ChooserDialog().with(StudentActivity.this)
                        .withStartFile("/storage/emulated/0/")
                        .withChosenListener(new ChooserDialog.Result() {
                            @Override
                            public void onChoosePath(String path, File pathFile) {
                                try {
                                    if (path.endsWith(".xlsx")) {
                                        readFileXLSX(pathFile);
                                    } else {
                                        readFileXLS(pathFile);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (InvalidFormatException e) {
                                    e.printStackTrace();
                                }

                            }
                        })
                        .build()
                        .show();
            }
        });

        fab_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                {
                    final Dialog dialog = new Dialog(StudentActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_insert_student);

                    dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    final EditText id_student = dialog.findViewById(R.id.id_student);
                    final EditText name_student = dialog.findViewById(R.id.name_student);
                    final EditText name_class = dialog.findViewById(R.id.name_class);
                    Button btn_cancel = dialog.findViewById(R.id.btn_cancel);
                    Button btn_insert = dialog.findViewById(R.id.btn_insert_student);

                    btn_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    btn_insert.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int id = Integer.parseInt(id_student.getText().toString());
                            String name = name_student.getText().toString();
                            String nClass = name_class.getText().toString();
                            if (name == null || name.equals("") || nClass == null || nClass.equals("") ||
                                    id_student.getText() == null || id_student.equals("")) {
                                Toast.makeText(StudentActivity.this, "تأكد من تعبئة الحقول", Toast.LENGTH_SHORT).show();
                            } else {
                                insertStudentDatabase(id, name, nClass, getBaseContext());
                            }
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            }
        });
    }

    public ArrayList<Student> returnn(String sizeArray) {
        newList = new ArrayList<Student>();

        progressBar_student.setVisibility(View.VISIBLE);

        Query     query = FirebaseDatabase.getInstance().getReference().child("database").child("students")
                    .orderByKey();


        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot as : dataSnapshot.getChildren()) {
                    Student student = as.getValue(Student.class);
                    newList.add(student);
                }
                adapter.addAll(newList);
                mIsLoading = false;
                progressBar_student.setVisibility(View.GONE);

                if (adapter.getItemCount() > 0) {
                    text_check.setVisibility(View.GONE);
                } else {
                    text_check.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mIsLoading = false;
            }
        });
        return newList;
    }

    private void insertStudentDatabase(final int id, final String name, final String nClass,final Context mContext) {
        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("students")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                text_check.setVisibility(View.GONE);
                FirebaseDatabase.getInstance().getReference().child("database").child("students")
                        .orderByChild("id").equalTo(id)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                                if (dataSnapshot2.exists()) {
                                    Toast.makeText(mContext, "هذا الطالب لديه نفس الرقم الهوية المسجل في قاعدة بياناتنا", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (dataSnapshot.getChildrenCount() == 0) {
                                        Student student = new Student(id, name, nClass, 1);
                                        String id_row = "1";
                                        root.child(id_row).setValue(student);
                                        adapter.addStudent(student);
                                    } else {
                                        int size = 0;
                                        for (DataSnapshot as : dataSnapshot.getChildren()) {
                                            Student st = as.getValue(Student.class);
                                            size = st.getId_row() + 1;
                                        }
                                        Student student = new Student(id, name, nClass, size);
                                        root.child(size + "").setValue(student);
                                        adapter.addStudent(student);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    private void readFileXLS(final File pathFile) throws IOException, InvalidFormatException {
        mIsLoading = true;
        progressBar_student.setVisibility(View.VISIBLE);
        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("students")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int indexRow = 0;
                if (dataSnapshot.getChildrenCount() == 0) {
                    indexRow = 0;
                } else {
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Student st = as.getValue(Student.class);
                        indexRow = st.getId_row();
                    }

                }
                Workbook wb = null;
                try {
                    wb = WorkbookFactory.create(new FileInputStream(pathFile));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvalidFormatException e) {
                    e.printStackTrace();
                }
                Sheet mySheet = wb.getSheetAt(0);

                System.out.println(mySheet.getRow(1).getCell(0));

                Iterator<Row> rowIter = mySheet.rowIterator();

                int rowno = 0;
                while (rowIter.hasNext()) {
                    HSSFRow myRow = (HSSFRow) rowIter.next();

                    if (rowno != 0) {
                        int id = 0;
                        String name_class = null, name_student = null;

                        Iterator<Cell> cellIter = myRow.cellIterator();

                        while (cellIter.hasNext()) {
                            HSSFCell myCell = (HSSFCell) cellIter.next();

                            Log.e("22TAG", " Index :" + myCell.getColumnIndex() + " -- " + myCell.toString());

                            switch (myCell.getColumnIndex()) {
                                case 0:
                                    id = (int) Float.parseFloat(myCell.toString());
                                    break;
                                case 1:
                                    name_student = myCell.toString();

                                    break;
                                case 2:
                                    name_class = myCell.toString();

                                    break;

                            }
                        }


                        Student student = new Student(id, name_student, name_class, indexRow);
                        root.child(indexRow + "").setValue(student);

                    }

                    indexRow++;
                    rowno++;
                }

                progressBar_student.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn("");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressBar_student.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn("");
            }
        });


    }

    private void readFileXLSX(final File filename) throws IOException, InvalidFormatException {

        mIsLoading = true;
        progressBar_student.setVisibility(View.VISIBLE);

        Query query = FirebaseDatabase.getInstance().getReference().child("database").child("students")
                .orderByKey()
                .limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int indexRow = 0;
                if (dataSnapshot.getChildrenCount() == 0) {
                    indexRow = 0;
                } else {
                    for (DataSnapshot as : dataSnapshot.getChildren()) {
                        Student st = as.getValue(Student.class);
                        indexRow = st.getId_row();
                    }

                }
                XSSFWorkbook wb = null;
                try {
                    wb = new XSSFWorkbook(new FileInputStream(filename));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Sheet mySheet = wb.getSheetAt(0);
                Iterator<Row> rowIterator = mySheet.iterator();
                int rowno = 0;
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    if (rowno != 0) {
                        int id = 0;
                        String name_class = null, name_student = null;
                        Iterator<Cell> cellIterator = row.cellIterator();
                        int colno = 0;
                        while (cellIterator.hasNext()) {
                            Cell cell = cellIterator.next();
                            colno++;
                            Log.e("22TAG", " Index :" + cell.getColumnIndex() + " -- " + cell.toString());
                            switch (cell.getColumnIndex()) {
                                case 0:
                                    id = (int) Float.parseFloat(cell.toString());
                                    break;
                                case 1:
                                    name_student = cell.toString();

                                    break;
                                case 2:
                                    name_class = cell.toString();
                                    break;
                            }
                        }
                        Student student = new Student(id, name_student, name_class, indexRow);
                        root.child(indexRow + "").setValue(student);
                    }
                    rowno++;
                    indexRow++;
                }

                progressBar_student.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn("");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressBar_student.setVisibility(View.GONE);
                mIsLoading = false;
                adapter.reListData();
                returnn("");        }
        });



    }


    private void initView() {
        Drawable upArrow = getResources().getDrawable(R.drawable.ic_forward);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("الطلاب");

        text_check = findViewById(R.id.text_check);
        recycler_student = findViewById(R.id.recycler_student);
        progressBar_student = findViewById(R.id.progressBar_student);
        fab_insert = findViewById(R.id.fab_add);
        import_to_app = findViewById(R.id.import_to_app);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        root = database.getReference("database").child("students");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}