package com.mhammed_khaled_alseqaly.library_temohammed.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.TextView;

import com.mhammed_khaled_alseqaly.library_temohammed.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        TextView textView = findViewById(R.id.text_app);
        TextView name1 = findViewById(R.id.name1);
        TextView name2 = findViewById(R.id.name2);

        Typeface face = Typeface.createFromAsset(getAssets(),
                "Omar-Bold.ttf");

        Typeface face2 = Typeface.createFromAsset(getAssets(),
                "Omar-Regular.ttf");

        textView.setTypeface(face);
        name1.setTypeface(face2);
        name2.setTypeface(face2);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        }, 1000);

    }
}